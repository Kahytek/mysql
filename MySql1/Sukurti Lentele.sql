USE prekes;

CREATE TABLE prekes (
	Kaina FLOAT(5,2),
    MatVnt VARCHAR(1000),
    Pavadinimas VARCHAR(150),
    Barkodas CHAR(12)
);