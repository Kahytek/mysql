DROP TABLE Child;
DROP TABLE Parent;

CREATE TABLE Parent (
ParentID int primary key auto_increment,
AA int UNIQUE,
BB INT
);

CREATE TABLE Child (
ChildID int primary key auto_increment,
ParentID int,
AA int,
FOREIGN KEY (ParentID) references Parent(ParentID),
foreign key (AA) references Parent(AA)
	ON UPDATE CASCADE 
);

insert into Parent(AA, BB)
values (1, 2), (2, 3), (3, 4), (5, 6), (7, 8);

insert into Child(ParentID, AA)
values (1,1), (2,2), (3,5), (4,7), (5,5);

SELECT * FROM Parent;
SELECT * FROM Child;

update parent set AA = 4 where ParentID = 3;
SELECT * FROM Child;


