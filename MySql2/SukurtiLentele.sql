USE Gatve;

drop table stulpai;
drop table gatves;

CREATE TABLE gatves (
    ID int NOT NULL PRIMARY KEY auto_increment,
    Pavadinimas VARCHAR(100)
);
#ALTER TABLE gatves AUTO_INCREMENT=100;

CREATE TABLE stulpai (
	ID int NOT NULL PRIMARY KEY auto_increment,
	Stulpas VARCHAR(100),
    GatvesID int,
    FOREIGN KEY (GatvesID) REFERENCES gatves(ID)
	#ON DELETE CASCADE
    ON DELETE SET NULL
);


    

CREATE TABLE moksleiviai (
	ID int PRIMARY KEY AUTO_INCREMENT,
	Moksleivis VARCHAR(20)
);

CREATE TABLE dalykai(
	ID int PRIMARY KEY AUTO_INCREMENT,
	dalykas VARCHAR(20)    
);

CREATE TABLE moksleiviuDalykai(
	MoksleivioID int,
	DalykoId int,
	FOREIGN KEY (MoksleivioID) REFERENCES moksleiviai(ID),
	FOREIGN KEY (DalykoID) REFERENCES dalykai(ID)
);



#ALTER TABLE stulpai AUTO_INCREMENT=100;



