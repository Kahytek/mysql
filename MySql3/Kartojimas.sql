CREATE SCHEMA `Gatve`;

CREATE SCHEMA `MySql3`;
USE MySql3;

DROP TABLE Uzsakymas2;
DROP TABLE Naudotojas2;


CREATE TABLE Naudotojas (
ID int primary key auto_increment,
Vardas Varchar(100)

);

CREATE TABLE Uzsakymas (
ID int primary key auto_increment,
Numeris Varchar(100),
NaudotojoID int,
FOREIGN KEY (NaudotojoID) references Naudotojas(ID)
	ON DELETE CASCADE
);

INSERT INTO Naudotojas (Vardas)
Values ('Vardas1'), ('Vardas2'), ('Vardas3');

INSERT INTO Uzsakymas(NaudotojoID, Numeris)
Values (1, 'ZP-1234'), (2, 'ZP-1235'), (3, 'ZP-1236');

Select * FROM Naudotojas;
Select * FROM Uzsakymas;

CREATE TABLE Naudotojas2 (
ID int primary key auto_increment,
Vardas Varchar(100),
AdresoID int unique
);

CREATE TABLE Uzsakymas2 (
ID int primary key auto_increment,
Numeris Varchar(100),
NaudotojoID int,
AdresoID int,
FOREIGN KEY (NaudotojoID) references Naudotojas(ID)
	ON UPDATE SET NULL,
FOREIGN KEY (AdresoID) references Naudotojas(AdresoID)    
	ON UPDATE CASCADE
);

INSERT INTO Naudotojas2 (Vardas, AdresoID)
Values ('Vardas1', 1), ('Vardas2', 2), ('Vardas3', 3);

INSERT INTO Uzsakymas(NaudotojoID, Numeris, AdresoID)
Values (1, 'ZP-1234', 3), (2, 'ZP-1235', 1), (3, 'ZP-1236', 1);







