create schema `parduotuve`;

USE parduotuve;

create table parduotuves (
ParduotuvesId int primary key auto_increment,
Adresas varchar(100)
);

create table kasos (
KasosID int primary key auto_increment,
ParduotuvesID int,
KasosNumeris varchar(30),
foreign key (ParduotuvesID) references parduotuves(ParduotuvesID)
);

create table Kvitai (
KvitoID int primary key auto_increment,
Numeris int,
Suma int,
KasosID int,
foreign key (KasosID) references kasos(KasosID)
on delete cascade
);

insert into parduotuves (Adresas)
values ('adresas1'), ('adresas2'), ('adresas3'), ('adresas4');

insert into kasos (ParduotuvesID, KasosNumeris)
values
(1, '111'), (2, '222'), (3, '333'), (4, '444'), (1, '555'), (2, '666'), (3, '777'), (4, '888');

insert into Kvitai (Numeris, Suma, KasosID)
values
(111, 20, 2), (222, 50, 2), (333, 50, 3), (444, 60, 4), (555, 60, 5), (666, 60, 6), (777, 60, 7),  (999, 60, 5), (999, 60, 1);

select * from kasos;


select * from Kvitai;

select * from Kvitai
Where KasosID in (2, 4, 7);

select sum(Suma) from Kvitai Where Suma Between 40.08 and 44.38;

select * from Kvitai
where Numeris Like '___00_-%';

select * from Parduotuves
inner join Kasos ON Parduotuves.ParduotuvesID = Kasos.ParduotuvesID;

insert into Parduotuves(Adresas) values('Adresas5');

select * from Parduotuves
left join Kasos ON Parduotuves.ParduotuvesID = Kasos.ParduotuvesID;

select * from Parduotuves p
inner join Kasos k ON Parduotuves.ParduotuvesID = k.ParduotuvesID
inner join Kvitai kv ON K.KasosID = kv.KasosID;