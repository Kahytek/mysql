CREATE SCHEMA Biblioteka;

Use Biblioteka;

CREATE TABLE Bibliotekos(
ID int NOT NULL PRIMARY KEY auto_increment,
Pavadinimas varchar(100),
Adresas varchar(100),
TelNr varchar(50),
ElPastas varchar(50)
);

CREATE TABLE Skaitytojai(
ID int NOT NULL PRIMARY KEY auto_increment,
Vardas varchar(50),
Pavarde varchar(50),
TelNr varchar(50),
LankomosBibliotekosID int,
Foreign Key (LankomosBibliotekosID) references Biblioteka(ID)
);

CREATE TABLE Knygos(
ID int NOT NULL PRIMARY KEY auto_increment,
Pavadinimas varchar(50),
Autorius varchar(50),
Leidyka varchar(50),
LeidimoData varchar(50),
Kiekis int,
BibliotekosID int,
Foreign Key (BibliotekosID) references Biblioteka(ID)
);

CREATE TABLE Zurnalai(
ID int NOT NULL PRIMARY KEY auto_increment,
Pavadinimas varchar(50),
Numeris int,
Leidyka varchar(50),
LeidimoData varchar(50),
Kiekis int,
BibliotekosID int,
Foreign Key (BibliotekosID) references Biblioteka(ID)
);

CREATE TABLE PaimtosKnygos (
KnygosID int,
SkaitytojoID int,
Foreign Key (KnygosID) references Knygos(ID),
Foreign Key (SkaitytojoID) references Skaitytojai(ID)

);

#ISTAISYTI
